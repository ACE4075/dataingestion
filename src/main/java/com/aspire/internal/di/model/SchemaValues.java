package com.aspire.internal.di.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "schema_values")
public class SchemaValues implements Serializable {
	private static final long serialVersionUID = 1L;
	private String field_value, rel_id;
	private int id, field_id;

	@Id
	@Column(name = "id", unique = true, nullable = false, insertable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "field_value")
	public String getField_value() {
		return field_value;
	}

	public void setField_value(String field_value) {
		this.field_value = field_value;
	}

	@Column(name = "field_id")
	public int getField_id() {
		return field_id;
	}

	public void setField_id(int field_id) {
		this.field_id = field_id;
	}

	@Column(name = "rel_id")
	public String getRel_id() {
		return rel_id;
	}

	public void setRel_id(String rel_id) {
		this.rel_id = rel_id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SchemaValues other = (SchemaValues) obj;
		if (field_id != other.field_id)
			return false;
		if (field_value == null) {
			if (other.field_value != null)
				return false;
		} else if (!field_value.equals(other.field_value))
			return false;
		if (id != other.id)
			return false;
		if (rel_id == null) {
			if (other.rel_id != null)
				return false;
		} else if (!rel_id.equals(other.rel_id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return id + " " + field_id + " " + field_value + " " + rel_id;
	}

}
