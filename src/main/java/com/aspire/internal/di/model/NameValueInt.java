package com.aspire.internal.di.model;

import java.io.Serializable;
import java.util.List;

public class NameValueInt implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private List<String> aceno;

	private int value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public List<String> getAceno() {
		return aceno;
	}

	public void setAceno(List<String> aceno) {
		this.aceno = aceno;
	}

}
