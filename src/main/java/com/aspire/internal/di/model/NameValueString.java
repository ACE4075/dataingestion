package com.aspire.internal.di.model;

import java.io.Serializable;

public class NameValueString implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name, value, ace_number;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAce_number() {
		return ace_number;
	}

	public void setAce_number(String ace_number) {
		this.ace_number = ace_number;
	}

}
