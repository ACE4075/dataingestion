package com.aspire.internal.di.model;

public class DateToYMD {
	private int years, months, days;

	public int getYears() {
		return years;
	}

	public void setYears(int years) {
		this.years = years;
	}

	public int getMonths() {
		return months;
	}

	public void setMonths(int months) {
		this.months = months;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	@Override
	public String toString() {
		return "DateToYMD [years=" + years + ", months=" + months + ", days=" + days + "]";
	}

}
