package com.aspire.internal.di.model;

import java.io.Serializable;

public class NameValueDouble implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private double value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
}
