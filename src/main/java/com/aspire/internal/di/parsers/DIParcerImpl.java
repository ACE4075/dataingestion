package com.aspire.internal.di.parsers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.aspire.internal.di.model.ColumnNames;
import com.aspire.internal.di.model.SchemaValues;
import com.aspire.internal.di.service.IDIService;

@Configuration
public class DIParcerImpl implements IDIParser {
	
	@Autowired
	IDIService service;
	
	private static Map<String,Integer> colsMap;
	final static Logger logger = Logger.getLogger(IDIParser.class);
	
	@Override
	public List<String> getColumnNames(File file) {
		Workbook workbook = null;
		FileInputStream fileInput = null;
		Set<String> colNames = new LinkedHashSet<>();
		List<String> colList = new LinkedList<>();
		colNames.add("sheet_name");
		colNames.add("upload_date");
		Sheet sheet;
		Row row;
		try {
			fileInput = new FileInputStream(file);
			workbook = new XSSFWorkbook(fileInput);
		} catch (FileNotFoundException e1) {
			logger.debug("Exception in getColumnNames FileNotFoundException", e1);
		} catch (IOException e) {
			logger.debug("Exception in getColumnNames IOException", e);
		}
		for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
			sheet = workbook.getSheetAt(i);
			for (int colno = 0; colno < sheet.getRow(0).getLastCellNum(); colno++) {
				row = sheet.getRow(0);
				if (row != null) {
					Cell cell = row.getCell(colno);
					if (cell != null) {
						try {
							if (!colNames.contains(cell.getStringCellValue()))
								colNames.add(cell.getStringCellValue());
						} catch (Exception e) {
							logger.debug("Exception in getColumnNames while getting cell value", e);
						}
					}
				}
			}
		}
		try {
			workbook.close();
			fileInput.close();
		} catch (IOException e) {
			logger.debug("Exception in getColumnNames while closing", e);
		}
		colList.addAll(colNames);
		return colList;
	}

	public List<List<List< SchemaValues>>> parseDataRowWise(File file) {
		getColumnMap();
		Map<String, String> oneRecord = null;
		Workbook workbook = null;
		FileInputStream fileInput = null;
		List<List< SchemaValues>> keyValList;
		List<List<List< SchemaValues>>> ll = new LinkedList<>();
		List< SchemaValues> list=null;
		SchemaValues schemaValues;
		Sheet sheet;
		Row row;
		Calendar calUploadDate = Calendar.getInstance();
		calUploadDate.setTime(new Date());
		String uploadDate = calUploadDate.get(Calendar.DATE) + "-"
				+ (calUploadDate.get(Calendar.MONTH) + 1) + "-" + calUploadDate.get(Calendar.YEAR);
		try {
			fileInput = new FileInputStream(file);
			workbook = new XSSFWorkbook(fileInput);
		} catch (FileNotFoundException e1) {
			logger.debug("Exception in parseDataRowWise FileNotFoundException", e1);
		} catch (IOException e) {
			logger.debug("Exception in parseDataRowWise IOException", e);
		}
		for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
			sheet = workbook.getSheetAt(i);
			keyValList = new LinkedList<>();
			for (int rowNo = 1; sheet.getRow(rowNo) != null; rowNo++) {
				String uniqueID = "";
				oneRecord = new HashMap<>();
				for (int colNo = 0; colNo < sheet.getRow(0).getLastCellNum(); colNo++) {
					schemaValues = new SchemaValues();
					String stringValue = null;
					Date dateValue = null;
					Double numericValue = null;
					row = sheet.getRow(rowNo);
					if (row != null) {
						Cell cell = row.getCell(colNo);
						if (cell != null) {
							try {
								stringValue = cell.getStringCellValue();
								schemaValues.setField_value(stringValue);
							} catch (Exception e) {
								try {
									if (DateUtil.isCellDateFormatted(cell)) {
										dateValue = cell.getDateCellValue();
										Calendar cal = Calendar.getInstance();
										cal.setTime(dateValue);
										String formatedDate = cal.get(Calendar.DATE) + "-"
												+ (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
										schemaValues.setField_value(formatedDate);
									} else {
										numericValue = cell.getNumericCellValue();
										schemaValues.setField_value(numericValue.toString());
									}
								} catch (Exception e1) {
									logger.debug("Exception in parseDataRowWise while getting cell value Exception", e1);
								}
							}
							oneRecord.put(sheet.getRow(0).getCell(colNo).getStringCellValue(),
									schemaValues.getField_value());
						}
					}
				}
				uniqueID = oneRecord.get("ACENumber") ;
				list=new LinkedList<>();
				for (Map.Entry<String, String> entry : oneRecord.entrySet()) {
					schemaValues = new SchemaValues();
					schemaValues.setField_id(colsMap.get(entry.getKey()));//changes
					schemaValues.setField_value(entry.getValue());
					schemaValues.setRel_id(uniqueID);
					list.add(schemaValues);
				}
				schemaValues = new SchemaValues();
				schemaValues.setField_id(colsMap.get("sheet_name"));//changes
				schemaValues.setField_value(sheet.getSheetName());
				schemaValues.setRel_id(uniqueID);
				list.add(schemaValues);
				
				schemaValues = new SchemaValues();
				schemaValues.setField_id(colsMap.get("upload_date"));//changes
				schemaValues.setField_value(uploadDate);
				schemaValues.setRel_id(uniqueID);
				list.add(schemaValues);
				
				keyValList.add(list);
				oneRecord = null;
				//System.out.println(keyValList);
			}
			ll.add(keyValList);
		}
		try {
			workbook.close();
			fileInput.close();
		} catch (IOException e) {
			logger.debug("Exception in parseDataRowWise while closing IOException", e);
		}
		return ll;
	}

	private void getColumnMap(){
		Map<String,Integer> map = new  HashMap<>();
		for (ColumnNames columns : service.getColumnNames()) {
			map.put(columns.getColumnName(), columns.getId());
		}
		colsMap= map;
	}
}
