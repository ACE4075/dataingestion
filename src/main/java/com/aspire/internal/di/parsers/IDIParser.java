package com.aspire.internal.di.parsers;

import java.io.File;
import java.util.List;

import com.aspire.internal.di.model.SchemaValues;

public interface IDIParser {

	public List<String> getColumnNames(File file);

	public List<List<List<SchemaValues>>> parseDataRowWise(File file);
}
