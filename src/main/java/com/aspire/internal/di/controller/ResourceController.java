package com.aspire.internal.di.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aspire.internal.di.model.NameValueDouble;
import com.aspire.internal.di.model.NameValueInt;
import com.aspire.internal.di.model.NameValueString;
import com.aspire.internal.di.service.IDIService;

@RestController
public class ResourceController {

	final static Logger logger = Logger.getLogger(ResourceController.class);
	@Autowired
	IDIService service;

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/countbydesignation")
	public List<NameValueInt> getCountByDesignation() {
		return service.totalHeadCountByDesignation();
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/timespentbyacenumber")
	public List<NameValueInt> timeSpentByAceNumber() {
		return service.timeSpentByAceNumber();
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "/timetrend")
	public List<NameValueDouble> timeSpentTrend(@Valid @RequestBody List<String> list) {
		return service.timeSpentTrend(list);
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/benchcountbydesignation")
	public List<NameValueInt> getBenchCountByDesignation() {
		return service.benchCountByDesignation();
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/headcount")
	public int headCount() {
		return service.totalHeadCount();
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/headcount/{by}")
	public List<NameValueInt> headcountBy(@PathVariable(value = "by") String string) {
		return service.headCountTrend(string);
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/utilmeasurable")
	public double utilMeasurable() {
		return service.utilizationMeasurable();
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/utilmeasurable/{by}")
	public List<NameValueDouble> utilMeasurableByMonths(@PathVariable(value = "by") String string) {
		return service.utilizationMeasurableTrend(string);
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/utilideal")
	public double utilIdeal() {
		return service.utilizationIdeal();
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/utilideal/{by}")
	public List<NameValueDouble> utilIdeal(@PathVariable(value = "by") String string) {
		return service.utilizationIdealTrend(string);
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/revenueleak")
	public double revenueLeak() {
		return service.revenueLeak();
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/revenueleak/{by}")
	public List<NameValueDouble> revenueLeak(@PathVariable(value = "by") String string) {
		return service.revenueLeakTrend(string);
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/benchage")
	public List<NameValueInt> benchAgeByDesignation() {
		return service.benchAgeByDesignation();
	}
	
	@CrossOrigin(origins = "*", maxAge = 3600)
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "/benchtrend")
	public List<NameValueString> benchAgeTrend(@Valid @RequestBody List<String> list) {
		return service.benchAgeTrend(list);
	}
	

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/uploadtrend/{by}")
	public List<NameValueInt> uploadTrend(@PathVariable(value = "by") String string) {
		return service.uploadTrend(string);
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@PostMapping(value = "/saveorupdate")
	public ResponseEntity<String> saveOrUpdate(@Valid MultipartFile file) {
		if (file == null |file.isEmpty() | !file.getOriginalFilename().endsWith("xlsx")) {
			return ResponseEntity.badRequest().build();
		} else {
			try {
				service.saveOrUpdate(file, null, null);
				return ResponseEntity.ok().body("Successfully done save or updating "+file.getOriginalFilename());
			} catch (Exception e) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
			}
		}
	}

	@CrossOrigin(origins = "*", maxAge = 3600)
	@PostMapping("/saveorupdate/{operator}/{month}")
	public ResponseEntity<Object> saveOrUpdateByCondition(MultipartFile file,
			@PathVariable(value = "operator") String operator, @PathVariable(value = "month") String month) {
		if (file == null | file.isEmpty() | !file.getOriginalFilename().endsWith("xlsx")) {
			return ResponseEntity.badRequest().build();
		} else {
			try {
				service.saveOrUpdate(file, operator, month);
				return ResponseEntity.ok().body("Successfully done save or updating "+file.getOriginalFilename());
			} catch (Exception e) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
			}
		}
	}
}