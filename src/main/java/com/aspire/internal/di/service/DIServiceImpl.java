package com.aspire.internal.di.service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import com.aspire.internal.di.dao.ColumnNamesRepository;
import com.aspire.internal.di.dao.SchemaValuesRepository;
import com.aspire.internal.di.exception.DiException;
import com.aspire.internal.di.model.ColumnNames;
import com.aspire.internal.di.model.DateToYMD;
import com.aspire.internal.di.model.NameValueDouble;
import com.aspire.internal.di.model.NameValueInt;
import com.aspire.internal.di.model.NameValueString;
import com.aspire.internal.di.model.SchemaValues;
import com.aspire.internal.di.parsers.IDIParser;

@Service
public class DIServiceImpl implements IDIService {

	@Autowired
	SchemaValuesRepository svrepo;

	@Autowired
	ColumnNamesRepository colRepo;

	@Autowired
	IDIParser parser;

	@Value("${column.upload_date}")
	private String upload_date;

	@Value("${column.date}")
	private String date;

	@Value("${column.ace_number}")
	private String ace_number;

	@Value("${column.emp_work_role}")
	private String emp_work_role;

	@Value("${column.time_spent}")
	private String time_spent;

	@Value("${column.billed_hours}")
	private String billed_hours;

	@Value("${time.first}")
	private String time_first;

	@Value("${time.second}")
	private String time_second;

	@Value("${time.third}")
	private String time_third;

	@Value("${bench.first}")
	private String bench_first;

	@Value("${bench.second}")
	private String bench_second;

	@Value("${bench.third}")
	private String bench_third;

	final static Logger logger = Logger.getLogger(IDIService.class);
	private final Path rootLocation = new File(".").toPath();

	@Override
	public void saveOrUpdate(MultipartFile file, String operator, String month) {
		try {
			if (file.isEmpty() && !file.getOriginalFilename().endsWith("xlsx")) {
				throw new DiException("File not valid" + file.getOriginalFilename());
			}
			Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()),
					StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {
			logger.error("Exception in copy of file");
			throw new DiException("Failed to save file " + file.getOriginalFilename());
		}

		try {
			if (operator != null && month != null)
				saveOrUpdateByCondition(new File(file.getOriginalFilename()), operator, month);
			else
				saveOrUpdate(new File(file.getOriginalFilename()));
		} catch (Exception e) {
			logger.error("Exception in saveOrUpdate");
			throw new DiException("Exception in saveOrUpdate of : " + file.getOriginalFilename());
		} finally {
			try {
				deleteFile(new File(file.getOriginalFilename()));
			} catch (Exception e) {
				logger.error("Exception while deleting file : " + file.getOriginalFilename());
				throw new DiException("Exception while deleting file : " + file.getOriginalFilename());
			}
		}
	}

	@Override
	public void deleteFile(File file) {
		try {
			FileSystemUtils.deleteRecursively(file);
		} catch (Exception e) {
			logger.error("Exception in deleteFile");
		}
	}

	@Override
	public void saveOrUpdateColumns(File fileName) {
		try {
			List<String> colsInXlsx = parser.getColumnNames(fileName);
			List<ColumnNames> colsInDb = colRepo.findAll();
			List<String> temp = new LinkedList<String>();
			for (ColumnNames cols : colsInDb) {
				temp.add(cols.getColumnName());
			}
			colsInXlsx.removeIf(temp::contains);
			for (String string : colsInXlsx) {
				colRepo.save(new ColumnNames(string));
			}
		} catch (Exception e) {
			logger.error("Exception in saveOrUpdateColumns");
			throw new DiException("Exception while saving column names : " + e);
		}
	}

	@Override
	public List<ColumnNames> getColumnNames() {
		try {
			return colRepo.findAll();
		} catch (Exception e) {
			logger.error("Exception in getColumnNames");
			return null;
		}
	}

	@Transactional
	public void saveOrUpdate(File file) {
		try {
			saveOrUpdateColumns(file);
			for (List<List<SchemaValues>> list : parser.parseDataRowWise(file))
				for (int i = 0; i < list.size(); i++) {
					List<SchemaValues> sv = svrepo.getByRelId(list.get(i).iterator().next().getRel_id());
					if (sv.size() != 0) {
						try {
							compareLists(sv, list.get(i));
						} catch (Exception e) {
							System.out.println("exception in updating");
						}
					} else {
						svrepo.save(list.get(i));
						if (i % 50 == 0)
							svrepo.flush();
					}
				}
		} catch (Exception e) {
			logger.error("Exception in saveOrUpdate");
			throw new DiException("Exception in saveOrUpdate: ");
		}
	}

	private boolean compareLists(List<SchemaValues> fromDb, List<SchemaValues> fromXl) {
		boolean flag = false;
		Map<Integer, SchemaValues> dbMap = new HashMap<>();
		Map<Integer, SchemaValues> xlMap = new HashMap<>();

		for (SchemaValues schemaValues : fromDb) {
			dbMap.put(schemaValues.getField_id(), schemaValues);
		}

		for (SchemaValues schemaValues : fromXl) {
			xlMap.put(schemaValues.getField_id(), schemaValues);
		}

		Set<Integer> xslKeys = xlMap.keySet();
		Set<Integer> dbKeys = dbMap.keySet();
		for (Integer key : dbKeys) {
			if (!dbMap.get(key).getField_value().equalsIgnoreCase(xlMap.get(key).getField_value())) {
				SchemaValues changes = dbMap.get(key);
				changes.setField_value(xlMap.get(key).getField_value());
				flag = true;
			}
		}
		xslKeys.removeIf(dbKeys::contains);
		for (Integer key : xslKeys) {
			SchemaValues changes = xlMap.get(key);
			svrepo.save(changes);
			flag = true;
		}
		return flag;
	}

	@Transactional
	public void saveOrUpdateByCondition(File file, String operator, String month) {
		try {
			if (operator == null | month == null) {
				saveOrUpdate(file);
			} else {
				saveOrUpdateColumns(file);
				for (List<List<SchemaValues>> list : parser.parseDataRowWise(file)) {
					List<List<SchemaValues>> filteredList = filterByCondition(list, operator, Integer.parseInt(month));
					for (int i = 0; i < filteredList.size(); i++) {
						List<SchemaValues> sv = svrepo.getByRelId(filteredList.get(i).iterator().next().getRel_id());
						if (sv.size() != 0) {
							try {
								compareLists(sv, filteredList.get(i));
							} catch (Exception e) {
								System.out.println("exception in updating");
							}
						} else {
							svrepo.save(filteredList.get(i));
						}
					}
					svrepo.flush();
				}
			}
		} catch (NumberFormatException e) {
			logger.error("Exception in saveOrUpdateByCondition");
		}
	}

	@Override
	public List<NameValueInt> totalHeadCountByDesignation() {
		List<NameValueInt> list = new LinkedList<>();
		try {
			int colId = getColumnId(emp_work_role);
			if (colId != -1) {
				List<Object[]> result = svrepo.countByDesignation(colId);
				NameValueInt values;
				for (int i = 0; i < result.size() && result != null; i++) {
					values = new NameValueInt();
					values.setName(result.get(i)[0].toString());
					values.setValue(Integer.parseInt(result.get(i)[1].toString()));
					list.add(values);
				}
			}
			return list;
		} catch (NumberFormatException e) {
			logger.error("Exception in totalHeadCountByDesignation");
			list.add(null);
			return list;
		}
	}

	@Override
	public List<NameValueInt> timeSpentByAceNumber() {
		List<NameValueInt> list = new LinkedList<>();
		try {
			List<String> ace_first = new LinkedList<>();
			List<String> ace_second = new LinkedList<>();
			List<String> ace_third = new LinkedList<>();
			List<Object[]> result = svrepo.timeSpentData(getColumnId(ace_number), getColumnId(time_spent));
			Map<String, Integer> map = new HashMap<>();
			NameValueInt values;
			String key = null;
			double value = 0;
			for (Object[] kv : result) {
				key = kv[0].toString();
				try {
					value = Double.parseDouble(kv[1].toString());
				} catch (Exception e) {
					value = 0;
				}
				if (value <= 60) {
					if (map.get(time_first) != null)
						map.put(time_first, map.get(time_first) + 1);
					else
						map.put(time_first, 1);
					ace_first.add(key);
				} else if (value > 60 && value <= 120) {
					if (map.get(time_second) != null)
						map.put(time_second, map.get(time_second) + 1);
					else
						map.put(time_second, 1);
					ace_second.add(key);
				} else if (value > 120) {
					if (map.get(time_third) != null)
						map.put(time_third, map.get(time_third) + 1);
					else
						map.put(time_third, 1);
					ace_third.add(key);
				}
			}
			for (Map.Entry<String, Integer> entrySet : map.entrySet()) {
				values = new NameValueInt();
				values.setName(entrySet.getKey());
				values.setValue(entrySet.getValue());
				if (entrySet.getKey().equalsIgnoreCase(time_first))
					values.setAceno(ace_first);
				else if (entrySet.getKey().equalsIgnoreCase(time_second))
					values.setAceno(ace_second);
				else if (entrySet.getKey().equalsIgnoreCase(time_third))
					values.setAceno(ace_third);
				list.add(values);
			}
			return list;
		} catch (NumberFormatException e) {
			logger.error("Exception in timeSpentByAceNumber", e);
			list.add(null);
			return list;
		}
	}

	@Override
	public List<NameValueDouble> timeSpentTrend(List<String> list) {
		String time;
		int colid = getColumnId(time_spent);
		double timeSpent;
		NameValueDouble nameValueDouble;
		List<NameValueDouble> nvList = new LinkedList<>();
		for (String ace : list) {
			try {
				time = svrepo.timeSpentForAceNumber(colid, ace);
				timeSpent = Double.parseDouble(time);
			} catch (Exception e) {
				timeSpent = 0;
				logger.error("Exception in timeSpentTrend");
			}
			nameValueDouble = new NameValueDouble();
			nameValueDouble.setName(ace);
			nameValueDouble.setValue(timeSpent);
			nvList.add(nameValueDouble);
		}
		return nvList;
	}

	@Override
	public List<NameValueInt> benchCountByDesignation() {
		NameValueInt nameValue;
		List<NameValueInt> list = new LinkedList<>();
		Map<String, Integer> map = new HashMap<>();
		int colId = getColumnId(emp_work_role);
		try {
			for (String designation : svrepo.benchCountByDesignation(colId)) {
				if (map.get(designation) != null)
					map.put(designation, map.get(designation) + 1);
				else
					map.put(designation, 1);
			}
			for (Map.Entry<String, Integer> entry : map.entrySet()) {
				nameValue = new NameValueInt();
				nameValue.setName(entry.getKey());
				nameValue.setValue(entry.getValue());
				list.add(nameValue);
			}
			return list;
		} catch (Exception e) {
			logger.error("Exception in benchCountByDesignation");
			list.add(null);
			return list;
		}
	}

	@Override
	public List<NameValueInt> benchAgeByDesignation() {
		List<String> ace_first = new LinkedList<>();
		List<String> ace_second = new LinkedList<>();
		List<String> ace_third = new LinkedList<>();
		Map<String, Integer> map = new HashMap<>();
		List<NameValueInt> list = new LinkedList<>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-M-yyyy");
		NameValueInt nameValues;
		int years, months, days;
		LocalDate startDate = null, today = LocalDate.now();
		Period period;
		String ace_no;
		DateToYMD ymd;
		try {
			for (Object[] values : svrepo.benchAge(getColumnId("Date"))) {
				try {
					startDate = LocalDate.parse(values[0].toString(), formatter);
				} catch (Exception e) {
					startDate = LocalDate.parse("1-1-2017", formatter);
				}
				ace_no = values[1].toString();
				period = Period.between(startDate, today);
				years = period.getYears();
				months = period.getMonths();
				days = period.getDays();
				ymd = new DateToYMD();
				if (years > 0)
					ymd.setYears(years);
				else if (months > 0)
					ymd.setMonths(months);
				if (days > 0)
					ymd.setDays(days);
				if (ymd.getYears() == 0 && ymd.getMonths() < 2) {
					if (map.get(bench_first) != null)
						map.put(bench_first, map.get(bench_first) + 1);
					else
						map.put(bench_first, 1);
					ace_first.add(ace_no);
				} else if (ymd.getYears() == 0 && ymd.getMonths() <= 4 && ymd.getMonths() >= 2 && ymd.getDays() > 0) {
					if (map.get(bench_second) != null)
						map.put(bench_second, map.get(bench_second) + 1);
					else
						map.put(bench_second, 1);
					ace_second.add(ace_no);
				} else if (ymd.getYears() > 0) {
					if (map.get(bench_third) != null)
						map.put(bench_third, map.get(bench_third) + 1);
					else
						map.put(bench_third, 1);
					ace_third.add(ace_no);
				}

			}
			for (Map.Entry<String, Integer> entrySet : map.entrySet()) {
				nameValues = new NameValueInt();
				nameValues.setName(entrySet.getKey());
				nameValues.setValue(entrySet.getValue());
				if (entrySet.getKey().equalsIgnoreCase(bench_first))
					nameValues.setAceno(ace_first);
				else if (entrySet.getKey().equalsIgnoreCase(bench_second))
					nameValues.setAceno(ace_second);
				else if (entrySet.getKey().equalsIgnoreCase(bench_third))
					nameValues.setAceno(ace_third);
				list.add(nameValues);
			}
			return list;
		} catch (Exception e) {
			logger.error("Exception in benchAgeByDesignation");
			list.add(null);
			return list;
		}
	}

	@Override
	public List<NameValueString> benchAgeTrend(List<String> list) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-M-yyyy");
		int years, months, days;
		LocalDate startDate, today = LocalDate.now();
		Period period;
		String totalCount;
		int date_colid = getColumnId(date);
		int design_colid = getColumnId(emp_work_role);
		NameValueString nameValue;
		List<NameValueString> nvList = new LinkedList<>();
		String[] values = null;
		for (String ace : list) {
			try {
				values = svrepo.benchAgeTrend(date_colid, design_colid, ace);
				totalCount = "";
				startDate = LocalDate.parse(values[0].substring(0, values[0].indexOf(",")), formatter);
				period = Period.between(startDate, today);
				years = period.getYears();
				months = period.getMonths();
				days = period.getDays();
				if (years > 0)
					totalCount = years + " years ";
				if (months > 0)
					totalCount += months + " Months ";
				if (days > 0)
					totalCount += days + " Days ";

				nameValue = new NameValueString();
				nameValue.setAce_number(ace);
				nameValue.setValue(totalCount);
				nameValue.setName(values[0].substring(values[0].indexOf(",") + 1));
				nvList.add(nameValue);
			} catch (Exception e) {
				logger.error("Exception in Bench age Trend");
			}
		}
		return nvList;
	}

	@Override
	public double utilizationMeasurable() {
		try {
			return svrepo.utilizationMeasurable(getColumnId(time_spent), getColumnId(billed_hours));
		} catch (Exception e) {
			logger.error("Exception in utilizationMeasurable");
			return -1;
		}
	}

	@Override
	public double utilizationIdeal() {
		try {
			return svrepo.utilizationIdeal(getColumnId(time_spent), getColumnId(billed_hours));
		} catch (Exception e) {
			logger.error("Exception in utilizationIdeal");
			return -1;
		}
	}

	@Override
	public double revenueLeak() {
		try {
			return svrepo.revenueLeak(getColumnId(time_spent), getColumnId(billed_hours));
		} catch (Exception e) {
			logger.error("Exception in revenueLeak");
			return -1;
		}
	}

	@Override
	public List<NameValueInt> uploadTrend(String string) {
		return null;
	}

	@Override
	public List<NameValueInt> headCountTrend(String string) {
		NameValueInt nameValue;
		List<NameValueInt> list = new LinkedList<>();
		Map<String, Integer> map = new HashMap<>();
		int currentYear_int = Calendar.getInstance().get(Calendar.YEAR);
		String currentYear = currentYear_int + "";
		try {
			for (String trend : svrepo.headCountTrend(getColumnId(date))) {
				if (string.equalsIgnoreCase("month") && trend.endsWith(currentYear)) {
					String month = getMonthValue(trend.substring(trend.indexOf("-") + 1, trend.lastIndexOf("-")));
					if (month != null) {
						if (map.get(month) != null)
							map.put(month, map.get(month) + 1);
						else
							map.put(month, 1);
					}
				} else if (string.equalsIgnoreCase("year")) {
					String year = trend.substring(trend.lastIndexOf("-") + 1);
					int year_int = Integer.parseInt(year);
					if (year != null && year_int > currentYear_int - 3) {
						if (map.get(year) != null)
							map.put(year, map.get(year) + 1);
						else
							map.put(year, 1);
					}
				}
			}
			if (map.isEmpty() && string.equalsIgnoreCase("month")) {
				for (String trend : svrepo.headCountTrend(getColumnId(date))) {
					if (trend.endsWith(currentYear_int - 1 + "")) {
						String month = getMonthValue(trend.substring(trend.indexOf("-") + 1, trend.lastIndexOf("-")));
						if (month != null) {
							if (map.get(month) != null)
								map.put(month, map.get(month) + 1);
							else
								map.put(month, 1);
						}
					}
				}
			}
			for (Map.Entry<String, Integer> entry : map.entrySet()) {
				nameValue = new NameValueInt();
				nameValue.setName(entry.getKey());
				nameValue.setValue(entry.getValue());
				list.add(nameValue);
			}
			return list;
		} catch (Exception e) {
			logger.error("Exception in hed count Trend");
			list.add(null);
			return list;
		}

	}

	private List<List<SchemaValues>> filterByCondition(List<List<SchemaValues>> list, String operator, int month) {
		List<List<SchemaValues>> returnList = new LinkedList<>();
		for (int i = 0; i < list.size(); i++) {
			for (SchemaValues schemaValues : list.get(i)) {
				if (schemaValues.getField_id() == getColumnId(date)) {
					String date = schemaValues.getField_value();
					if (!date.isEmpty()) {
						int mon = Integer.parseInt(date.substring(date.indexOf("-") + 1, date.lastIndexOf(("-"))));
						switch (operator.trim()) {
						case ">":
							if (mon > month)
								returnList.add(list.get(i));
							break;
						case "<":
							if (mon < month)
								returnList.add(list.get(i));
							break;
						case "=":
							if (mon == month)
								returnList.add(list.get(i));
							break;
						default:
							break;
						}
					}
				}
			}
		}
		return returnList;
	}

	private int getColumnId(String colName) {
		for (ColumnNames cols : getColumnNames()) {
			if (cols.getColumnName().equalsIgnoreCase(colName))
				return cols.getId();
		}
		return -1;
	}

	private String getMonthValue(String str) {
		switch (str) {
		case "1":
			return "JAN";
		case "2":
			return "FEB";
		case "3":
			return "MAR";
		case "4":
			return "APR";
		case "5":
			return "MAY";
		case "6":
			return "JUN";
		case "7":
			return "JUL";
		case "8":
			return "AUG";
		case "9":
			return "SEP";
		case "10":
			return "OCT";
		case "11":
			return "NOV";
		case "12":
			return "DEC";
		default:
			return "";
		}
	}

	@Override
	public int totalHeadCount() {
		try {
			return svrepo.getHeadCount();
		} catch (Exception e) {
			logger.debug("Exception in headcount", e);
			return -1;
		}
	}

	@Override
	public List<NameValueDouble> utilizationMeasurableTrend(String string) {
		List<NameValueDouble> nameValuesList = new LinkedList<>();
		NameValueDouble nameValues;
		try {
			List<NameValueDouble> timeSpent = getTrendData(
					svrepo.timeSpent(getColumnId(upload_date), getColumnId(time_spent)), string, true);
			List<NameValueDouble> billedHours = getTrendData(
					svrepo.billedHours(getColumnId(upload_date), getColumnId(billed_hours)), string, true);
			for (int i = 0; i < timeSpent.size(); i++) {
				nameValues = new NameValueDouble();
				nameValues.setName(timeSpent.get(i).getName());
				nameValues.setValue((timeSpent.get(i).getValue() / billedHours.get(i).getValue()) * 100);
				nameValuesList.add(nameValues);
			}
			return nameValuesList;
		} catch (Exception e) {
			logger.error("Exception in utilizationMeasurableTrend");
			return null;
		}
	}

	@Override
	public List<NameValueDouble> utilizationIdealTrend(String string) {
		List<NameValueDouble> nameValuesList = new LinkedList<>();
		NameValueDouble nameValues;
		try {
			List<NameValueDouble> timeSpent = getTrendData(
					svrepo.timeSpent(getColumnId(upload_date), getColumnId(time_spent)), string, false);
			List<NameValueDouble> billedHours = getTrendData(
					svrepo.billedHours(getColumnId(upload_date), getColumnId(billed_hours)), string, false);
			for (int i = 0; i < timeSpent.size(); i++) {
				nameValues = new NameValueDouble();
				nameValues.setName(timeSpent.get(i).getName());
				nameValues.setValue((timeSpent.get(i).getValue() / billedHours.get(i).getValue()) * 100);
				nameValuesList.add(nameValues);
			}
			return nameValuesList;
		} catch (Exception e) {
			logger.error("Exception in utilizationIdealTrend");
			return null;
		}
	}

	@Override
	public List<NameValueDouble> revenueLeakTrend(String string) {
		List<NameValueDouble> nameValuesList = new LinkedList<>();
		NameValueDouble nameValues;
		try {
			List<NameValueDouble> timeSpent = getTrendData(
					svrepo.timeSpent(getColumnId(upload_date), getColumnId(time_spent)), string, false);
			List<NameValueDouble> billedHours = getTrendData(
					svrepo.billedHours(getColumnId(upload_date), getColumnId(billed_hours)), string, false);
			for (int i = 0; i < timeSpent.size(); i++) {
				nameValues = new NameValueDouble();
				nameValues.setName(timeSpent.get(i).getName());
				nameValues.setValue((timeSpent.get(i).getValue() - billedHours.get(i).getValue()));
				nameValuesList.add(nameValues);
			}
			return nameValuesList;
		} catch (Exception e) {
			logger.error("Exception in revenueLeakTrend");
			return null;
		}
	}

	private List<NameValueDouble> getTrendData(List<Object[]> l1, String string, boolean utilMeasurable) {// ,boolean
		NameValueDouble nameValueDouble;
		List<NameValueDouble> list = new LinkedList<>();
		Map<String, Double> map = new HashMap<>();
		int currentYear_int = Calendar.getInstance().get(Calendar.YEAR);
		String date;
		double value;
		if (string.equalsIgnoreCase("year")) {
			for (int i = currentYear_int - 3; i <= currentYear_int; i++) {
				for (Object[] trend : l1) {
					date = trend[0].toString();
					try {
						value = Double.parseDouble(trend[1].toString());
					} catch (NumberFormatException e) {
						value = 0;
						logger.error("Exception in parsing string to double");
					}
					if (utilMeasurable) {
						if (value > 160.0)
							value = 160.0;
					}
					String year = date.substring(date.lastIndexOf("-") + 1);
					if (year.equalsIgnoreCase(i + "")) {
						if (year != null) {
							if (map.get(year) != null)
								map.put(year, map.get(year) + value);
							else
								map.put(year, value);
						}
					}
				}
			}
		} else if (string.equalsIgnoreCase("month")) {
			for (Object[] trend : l1) {
				date = trend[0].toString();
				try {
					value = Double.parseDouble(trend[1].toString());
				} catch (NumberFormatException e) {
					value = 0;
					logger.error("Exception in parsing string to double");
				}
				if (utilMeasurable) {
					if (value > 160)
						value = 160;
				}
				if (date.endsWith(currentYear_int + "")) {
					String month = getMonthValue(date.substring(date.indexOf("-") + 1, date.lastIndexOf("-")));
					if (month != null) {
						if (map.get(month) != null)
							map.put(month, map.get(month) + value);
						else
							map.put(month, value);
					}
				}
			}
		}

		if (map.isEmpty() && string.equalsIgnoreCase("month")) {
			for (Object[] trend : l1) {
				date = trend[0].toString();
				try {
					value = Double.parseDouble(trend[1].toString());
				} catch (NumberFormatException e) {
					logger.error("Exception in parsing string to double");
					value = 0;
				}
				if (utilMeasurable) {
					if (value > 160)
						value = 160;
				}
				if (date.endsWith(currentYear_int - 1 + "")) {
					String month = getMonthValue(date.substring(date.indexOf("-") + 1, date.lastIndexOf("-")));
					if (month != null) {
						if (map.get(month) != null)
							map.put(month, map.get(month) + value);
						else
							map.put(month, value);
					}
				}
			}
		}

		for (Map.Entry<String, Double> entry : map.entrySet()) {
			nameValueDouble = new NameValueDouble();
			nameValueDouble.setName(entry.getKey());
			nameValueDouble.setValue(entry.getValue());
			list.add(nameValueDouble);
		}
		return list;
	}

}
