package com.aspire.internal.di.service;

import java.io.File;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.aspire.internal.di.model.ColumnNames;
import com.aspire.internal.di.model.NameValueDouble;
import com.aspire.internal.di.model.NameValueInt;
import com.aspire.internal.di.model.NameValueString;

public interface IDIService {
	public void saveOrUpdate(MultipartFile file, String operator, String month);

	public void deleteFile(File file);

	public void saveOrUpdateColumns(File file);

	public void saveOrUpdate(File file);

	public void saveOrUpdateByCondition(File file, String operator, String month);

	public List<ColumnNames> getColumnNames();

	public List<NameValueInt> totalHeadCountByDesignation();

	public List<NameValueInt> timeSpentByAceNumber();

	public List<NameValueDouble> timeSpentTrend(List<String> list);

	public List<NameValueInt> benchAgeByDesignation();
	
	public List<NameValueString> benchAgeTrend(List<String> list);

	public List<NameValueInt> benchCountByDesignation();

	public List<NameValueInt> uploadTrend(String string);

	public int totalHeadCount();

	public List<NameValueInt> headCountTrend(String string);

	public double utilizationMeasurable();

	public List<NameValueDouble> utilizationMeasurableTrend(String string);

	public double utilizationIdeal();

	public List<NameValueDouble> utilizationIdealTrend(String string);

	public double revenueLeak();

	public List<NameValueDouble> revenueLeakTrend(String string);
}
