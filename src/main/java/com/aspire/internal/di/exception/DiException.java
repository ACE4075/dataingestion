package com.aspire.internal.di.exception;

public class DiException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DiException(String message) {
        super(message);
    }

    public DiException(String message, Throwable cause) {
        super(message, cause);
    }
}
