package com.aspire.internal.di.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.aspire.internal.di.model.SchemaValues;

public interface SchemaValuesRepository extends JpaRepository<SchemaValues, Long> {

	@Query(value = "select * from schema_values where rel_id=?1", nativeQuery = true)
	public List<SchemaValues> getByRelId(String relId);

	@Query(value = "select count(distinct(rel_id)) from schema_values", nativeQuery = true)
	public int getHeadCount();

	@Query(value = "select field_value,count(field_value) from schema_values  where field_id=?1 group by field_value", nativeQuery = true)
	public List<Object[]> countByDesignation(int id);

	@Query(value = "SELECT aceid.rel_id,role.field_value FROM schema_values as aceid JOIN schema_values AS role on aceid.rel_id=role.rel_id where aceid.field_id=?1 and role.field_id=?2", nativeQuery = true)
	public List<Object[]> timeSpentData(int ace, int timeSpent);

	@Query(value = "SELECT field_value FROM schema_values where field_id=?1", nativeQuery = true)
	public List<String> headCountTrend(int date);

	@Query(value = "SELECT a.field_value FROM schema_values as a JOIN schema_values AS b on a.rel_id=b.rel_id where a.field_id=?1 and b.field_value='bench'", nativeQuery = true)
	public List<String> benchCountByDesignation(int role);

	@Query(value = "SELECT sum(a.field_value)/sum(CASE WHEN b.field_value > 160 THEN 160 ELSE b.field_value END)*100 FROM schema_values as a JOIN schema_values AS b on a.rel_id=b.rel_id where a.field_id=?1 and b.field_id=?2", nativeQuery = true)
	public double utilizationMeasurable(int timeSpent, int billedHours);

	@Query(value = "SELECT sum(a.field_value)/sum(b.field_value)*100 FROM schema_values as a JOIN schema_values AS b on a.rel_id=b.rel_id where a.field_id=?1 and b.field_id=?2", nativeQuery = true)
	public double utilizationIdeal(int timeSpent, int billedHours);

	@Query(value = "SELECT sum(a.field_value)-sum(b.field_value) FROM schema_values as a JOIN schema_values AS b on a.rel_id=b.rel_id where a.field_id=?1 and b.field_id=?2", nativeQuery = true)
	public double revenueLeak(int timeSpent, int billedHours);

	@Query(value = "SELECT a.field_value as date,c.rel_id as ace_id FROM schema_values a,schema_values c where a.field_id=?1 and c.field_value='Bench'and a.rel_id=c.rel_id", nativeQuery = true)
	public List<Object[]> benchAge(int date);

	@Query(value = "SELECT a.field_value as date,b.field_value as designation FROM schema_values a,schema_values b where a.field_id=?1 and b.field_id=?2 and a.rel_id=?3 and b.rel_id=?3", nativeQuery = true)
	public String[] benchAgeTrend(int date, int designation, String ace_no);

	@Query(value = "select field_value from schema_values  where field_id=?1 group by rel_id", nativeQuery = true)
	public List<String> uploadTrend(int date);

	@Query(value = "select a.field_value as date,b.field_value as timespent from schema_values as a, schema_values as b where a.field_id=?1 and b.field_id=?2 and a.rel_id=b.rel_id", nativeQuery = true)
	public List<Object[]> timeSpent(int uploadDate, int timeSpent);

	@Query(value = "select a.field_value as date,b.field_value as billedhours from schema_values as a, schema_values as b where a.field_id=?1 and b.field_id=?2 and a.rel_id=b.rel_id", nativeQuery = true)
	public List<Object[]> billedHours(int uploadDate, int billedHours);

	@Query(value = "select a.field_value from schema_values as a where a.field_id=?1 and a.rel_id=?2 ", nativeQuery = true)
	public String timeSpentForAceNumber(int timeSpent, String aceNumber);

}
