package com.aspire.internal.di.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aspire.internal.di.model.ColumnNames;

@Repository
public interface ColumnNamesRepository extends JpaRepository<ColumnNames, String>	 {

}